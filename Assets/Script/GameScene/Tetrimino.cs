﻿using UnityEngine;
using System.Collections;

public abstract class Tetrimino : MonoBehaviour {

    protected Vector3[] blockPosition;

    protected Vector2 position;
    protected Vector2 tmp_offset;
    protected Vector3 center_position;
    public GameObject field;

    public bool next;
    public int gravitySpeed;
    static protected float blockSize = 0.32f;
    protected float gravityTimer;

	// Use this for initialization
	protected void Start () {
        tmp_offset = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	protected void Update () {

        if (!next)
        {
            gravityTimer += Time.deltaTime;
            if (gravityTimer >= 1.0f * gravitySpeed / 60.0f)
            {
               
                if(!Fall())
                {
                    Field fieldscript = field.GetComponent<Field>();
                    fieldscript.PutBlock((int)position.x + (int)blockPosition[0].x,
                        (int)position.y + (int)blockPosition[0].y, this.transform.FindChild("Block1").gameObject);
                    fieldscript.PutBlock((int)position.x + (int)blockPosition[1].x,
                        (int)position.y + (int)blockPosition[1].y, this.transform.FindChild("Block2").gameObject);
                    fieldscript.PutBlock((int)position.x + (int)blockPosition[2].x,
                        (int)position.y + (int)blockPosition[2].y, this.transform.FindChild("Block3").gameObject);
                    fieldscript.PutBlock((int)position.x + (int)blockPosition[3].x,
                        (int)position.y + (int)blockPosition[3].y, this.transform.FindChild("Block4").gameObject);
                    DestroyObject(this.gameObject);
                    fieldscript.CheckBlockAlign();
                }

                gravityTimer -= 1.0f;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Rotate();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                Move(1);
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Move(-1);
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                Fall();
            }
            float display_x = field.transform.position.x + (blockSize * (int)(position.x + tmp_offset.x)) + (blockSize / 2);
            float display_y = field.transform.position.y + (blockSize * (int)(position.y + tmp_offset.y)) + (blockSize / 2);
            transform.position = new Vector3(display_x, display_y, 1);
        }
    }

    public void Activate(int x, int y)
    {
        next = false;
        gravityTimer = 0;
        position.x = x;
        position.y = y;
    }

    public abstract Vector3 GetCenterPos();
    protected abstract bool HitCheck(int x, int y, bool rotate);
    protected abstract void Rotate();
    protected bool Fall()
    {
        if(tmp_offset.y > 0)
        {
            position.y += tmp_offset.y;
        }
        else if (!HitCheck((int)position.x, (int)position.y - 1, false))
        {
            position.y -= 1;
            return true;
        }

        return false;
    }
    protected abstract void Move(int x);
    //protected abstract GameObject GetBlock();



}
