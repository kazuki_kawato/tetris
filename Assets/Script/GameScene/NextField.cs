﻿using UnityEngine;
using System.Collections;

public class NextField : MonoBehaviour {

    public GameObject nextMino;
    public GameObject[] Tetriminos;
    protected float blockSize = 0.32f;
    
    // Use this for initialization
    public void Start () {
        nextMino = null;
	}
	
	// Update is called once per frame
	public void Update () {
	    if(nextMino == null) {
            GenerateTetrimino();
        }
	}

    void GenerateTetrimino() {
        GameObject obj = Tetriminos[Random.Range(0, 6)];
        Vector3 center = obj.GetComponent<Tetrimino>().GetCenterPos();
        nextMino = (GameObject)Instantiate(obj,
            center + transform.position, Quaternion.identity);
        
        nextMino.GetComponent<Tetrimino>().next = true;
        nextMino.GetComponent<Tetrimino>().field = gameObject;
    }

    public Tetrimino GetNextMino()
    {
        Tetrimino mino = nextMino.GetComponent<Tetrimino>();
        nextMino = null;
        return mino;
    }
}
