﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Field : MonoBehaviour {

    const int START_X = 5;
    const int START_Y = 20;


    public GameObject nextField;
    GameObject[,] blockAlign;
    Tetrimino current;
    protected float blockSize = 0.32f;

    // Use this for initialization
    void Start () {
        blockAlign = new GameObject[10, 24];

        for(int j=0; j<24; j++)
        {
            for(int i=0; i<10; i++)
            {
                blockAlign[i,j] = null;
            }
        }
        current = null;
	}
	
	// Update is called once per frame
	void Update () {

        GetTetrimino();
           
	}

    public void GetTetrimino() {
        if(current == null) {
            NextField next = nextField.GetComponent<NextField>();
            current = nextField.GetComponent<NextField>().GetNextMino();
            if(hitCheck(START_X, START_Y)){
                SceneManager.LoadScene("GameOverScene", LoadSceneMode.Additive);
                return;
            }
            current.Activate(START_X, START_Y);
            current.field = this.gameObject;
        }
    }

    public bool hitCheck(int x, int y)
    {
        if(x >= 10 || x < 0 || y < 0)
        {
            return true;
        }

        return (blockAlign[x, y] != null);
        
    }

    public void PutBlock(int x, int y, GameObject block_type)
    {
        float disp_x = transform.position.x + (blockSize * x) + (blockSize / 2);
        float disp_y = transform.position.y + (blockSize * y) + (blockSize / 2);
        blockAlign[x, y] = Instantiate(block_type, new Vector3(disp_x, disp_y, 1.0f), Quaternion.identity) as GameObject;

    }

    public void CheckBlockAlign()
    {
        for (int j = 0; j < 20; j++)
        {
            bool canDelete = true;
            for (int i = 0; i < 10; i++)
            {
                if (blockAlign[i, j] == null)
                {
                    canDelete = false;
                    break;
                }
            }

            if (canDelete)
            {
                DeleteLine(j);
                j--;
            }
        }
    }

    void DeleteLine(int y)
    {
        for(int i=0; i<10; i++)
        {
            DestroyObject(blockAlign[i, y]);
        }

        for (int i = 0; i < 10; i++)
        {
            for (int j = y; j < 20; j++)
            {
                blockAlign[i, j] = blockAlign[i, j + 1];
                if (blockAlign[i, j] != null)
                {
                    float disp_x = transform.position.x + (blockSize * i) + (blockSize / 2);
                    float disp_y = transform.position.y + (blockSize * j) + (blockSize / 2);
                    blockAlign[i, j].transform.position = new Vector3(disp_x, disp_y, 1.0f);
                }
            }
        }
    }
}
