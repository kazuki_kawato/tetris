﻿using UnityEngine;
using System.Collections;

public class O_Tetrimino : Tetrimino
{

    static Vector3 CENTER_POSITION = new Vector3(-(blockSize / 2), -(blockSize / 2), 0);
    // Use this for initialization
    void Awake()
    {
        blockPosition = new Vector3[] { new Vector3(0, 0), new Vector3(1, 0), new Vector3(0, 1), new Vector3(1, 1) };
    }

    // Update is called once per frame
    //void Update () {

    //}

    protected override void Rotate()
    {
    }

    protected override void Move(int x)
    {
        if (!HitCheck((int)position.x + x, (int)position.y, false))
        {
            position.x += x;
        }
    }

    public override Vector3 GetCenterPos()
    {
        return CENTER_POSITION;
    }

    protected override bool HitCheck(int x, int y, bool rotate)
    {
        Field fieldscript = field.GetComponent<Field>();
        
        if (!fieldscript.hitCheck(x + (int)blockPosition[0].x, y + (int)blockPosition[0].y) &&
            !fieldscript.hitCheck(x + (int)blockPosition[1].x, y + (int)blockPosition[1].y) &&
            !fieldscript.hitCheck(x + (int)blockPosition[2].x, y + (int)blockPosition[2].y) &&
            !fieldscript.hitCheck(x + (int)blockPosition[3].x, y + (int)blockPosition[3].y))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
