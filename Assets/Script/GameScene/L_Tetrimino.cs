﻿using UnityEngine;
using System.Collections;

public class L_Tetrimino : Tetrimino
{

    static Vector3 CENTER_POSITION = new Vector3(0, -(blockSize / 2), 0);
    // Use this for initialization
    void Awake()
    {
        blockPosition = new Vector3[] { new Vector3(1, 0), new Vector3(0, 0), new Vector3(-1, 0), new Vector3(1, 1) };
    }

    // Update is called once per frame
    //void Update () {

    //}

    protected override void Rotate()
    {
        tmp_offset.y = 0;
        if (!HitCheck((int)position.x, (int)position.y, true))
        {
            transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), 90.0f, Space.Self);
            int tmp;
            tmp = (int)blockPosition[0].y; blockPosition[0].y = blockPosition[0].x; blockPosition[0].x = -tmp;
            tmp = (int)blockPosition[1].y; blockPosition[1].y = blockPosition[1].x; blockPosition[1].x = -tmp;
            tmp = (int)blockPosition[2].y; blockPosition[2].y = blockPosition[2].x; blockPosition[2].x = -tmp;
            tmp = (int)blockPosition[3].y; blockPosition[3].y = blockPosition[3].x; blockPosition[3].x = -tmp;
        }
        else if(!HitCheck((int)position.x, (int)position.y + 1, true))
        {
            tmp_offset.y=1;
            transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), 90.0f, Space.Self);
            int tmp;
            tmp = (int)blockPosition[0].y; blockPosition[0].y = blockPosition[0].x; blockPosition[0].x = -tmp;
            tmp = (int)blockPosition[1].y; blockPosition[1].y = blockPosition[1].x; blockPosition[1].x = -tmp;
            tmp = (int)blockPosition[2].y; blockPosition[2].y = blockPosition[2].x; blockPosition[2].x = -tmp;
            tmp = (int)blockPosition[3].y; blockPosition[3].y = blockPosition[3].x; blockPosition[3].x = -tmp;
        }
        else if (!HitCheck((int)position.x - 1, (int)position.y, true))
        {
            transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), 90.0f, Space.Self);
            position.x -= 1;
            int tmp;
            tmp = (int)blockPosition[0].y; blockPosition[0].y = blockPosition[0].x; blockPosition[0].x = -tmp;
            tmp = (int)blockPosition[1].y; blockPosition[1].y = blockPosition[1].x; blockPosition[1].x = -tmp;
            tmp = (int)blockPosition[2].y; blockPosition[2].y = blockPosition[2].x; blockPosition[2].x = -tmp;
            tmp = (int)blockPosition[3].y; blockPosition[3].y = blockPosition[3].x; blockPosition[3].x = -tmp;
        }
        else if (!HitCheck((int)position.x + 1, (int)position.y, true))
        {
            transform.Rotate(new Vector3(0.0f, 0.0f, 1.0f), 90.0f, Space.Self);
            position.x += 1;
            int tmp;
            tmp = (int)blockPosition[0].y; blockPosition[0].y = blockPosition[0].x; blockPosition[0].x = -tmp;
            tmp = (int)blockPosition[1].y; blockPosition[1].y = blockPosition[1].x; blockPosition[1].x = -tmp;
            tmp = (int)blockPosition[2].y; blockPosition[2].y = blockPosition[2].x; blockPosition[2].x = -tmp;
            tmp = (int)blockPosition[3].y; blockPosition[3].y = blockPosition[3].x; blockPosition[3].x = -tmp;
        }
    }

    protected override void Move(int x)
    {
        if (!HitCheck((int)position.x + x, (int)position.y, false))
        {
            position.x += x;
        }
    }

    public override Vector3 GetCenterPos()
    {
        return CENTER_POSITION;
    }

    protected override bool HitCheck(int x, int y, bool rotate)
    {
        Field fieldscript = field.GetComponent<Field>();
        if (!rotate)
        {
            if (!fieldscript.hitCheck(x + (int)blockPosition[0].x, y + (int)blockPosition[0].y) &&
                !fieldscript.hitCheck(x + (int)blockPosition[1].x, y + (int)blockPosition[1].y) &&
                !fieldscript.hitCheck(x + (int)blockPosition[2].x, y + (int)blockPosition[2].y) &&
                !fieldscript.hitCheck(x + (int)blockPosition[3].x, y + (int)blockPosition[3].y))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if (!fieldscript.hitCheck(x - (int)blockPosition[0].y, y + (int)blockPosition[0].x) &&
                 !fieldscript.hitCheck(x - (int)blockPosition[1].y, y + (int)blockPosition[1].x) &&
                 !fieldscript.hitCheck(x - (int)blockPosition[2].y, y + (int)blockPosition[2].x) &&
                 !fieldscript.hitCheck(x - (int)blockPosition[3].y, y + (int)blockPosition[3].x))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
