﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        SceneManager.UnloadScene("GameScene");
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            SceneManager.LoadScene("OpScene");
            SceneManager.UnloadScene("GameOverScene");
        }
	}
}
