﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OpScene : MonoBehaviour {

    bool isLoading = false;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            Vector3 aTapPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D aCollider2d = Physics2D.OverlapPoint(aTapPoint);

            if (aCollider2d)
            {
                if(aCollider2d.transform.gameObject.name == "StartButton" && !isLoading)
                {
                    isLoading = true;
                    SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
                }
            }
        }
	}
}
